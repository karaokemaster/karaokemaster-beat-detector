# KarokeMaster

This project is part of [KaraokeMaster](karaokemaster/karaokemaster).

## Installing

```sh
systemctl --user stop KaraokeMaster-BeatDetector

sudo su
mkdir /opt/KaraokeMaster
mkdir /opt/KaraokeMaster/BeatDetector
pushd /opt/KaraokeMaster
curl --location --output KaraokeMaster.BeatDetector.zip "https://gitlab.com/karaokemaster/karaokemaster-beat-detector/-/jobs/artifacts/develop/raw/KaraokeMaster.BeatDetector.zip?job=package"
unzip -o KaraokeMaster.BeatDetector.zip -d /opt/KaraokeMaster/BeatDetector/
# ln -T ./BeatDetector/systemd.service /etc/systemd/system/KaraokeMaster-BeatDetector.service
chown -R pi:staff ./BeatDetector/
popd

exit

systemctl --user start KaraokeMaster-BeatDetector

cp /opt/KaraokeMaster/BeatDetector/systemd.service /home/pi/.local/share/systemd/user/KaraokeMaster-BeatDetector.service

systemctl daemon-reload
systemctl status KaraokeMaster-BeatDetector
systemctl start KaraokeMaster-BeatDetector
systemctl --user enable KaraokeMaster-BeatDetector

journalctl -u KaraokeMaster-BeatDetector
```
## License

KaraokeMaster is free software, provided under GPLv3.


