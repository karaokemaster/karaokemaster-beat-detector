FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

# Create app directory
WORKDIR /usr/src/karaokemaster-beatdetector

# Install crosspi toolchain
RUN apt-get update && apt-get install install gcc g++ make cmake wget unzip -y
RUN wget https://github.com/Pro/raspi-toolchain/releases/latest/download/raspi-toolchain.tar.gz
RUN tar xfz raspi-toolchain.tar.gz --strip-components=1 -C /opt
RUN export PATH=$PATH:/opt/cross-pi-gcc/bin

# Build libraries

# Switch to git clone
wget https://github.com/erikd/libsamplerate/archive/master.zip
unzip master.zip
rm master.zip
pushd libsamplerate-master
mkdir build
pushd build
cmake ..
make
popd
popd

wget https://github.com/adamstark/BTrack/archive/master.zip
unzip master.zip
rm master.zip
pushd BTrack-master/src

/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++ -DUSE_KISS_FFT -fPIC ../libs/kiss_fft130/kiss_fft.c BTrack.cpp OnsetDetectionFunction.cpp -shared -o libbtrack.so -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -l:libsamplerate.a

/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-gcc -c -fPIC ../libs/kiss_fft130/kiss_fft.c -o kiss_fft.o
/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++ -DUSE_KISS_FFT -c -fPIC BTrack.cpp -o BTrack.o -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -l:libsamplerate.a
/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++ -DUSE_KISS_FFT -c -fPIC OnsetDetectionFunction.cpp -o OnsetDetectionFunction.o -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -l:libsamplerate.a
/opt/cross-pi-gcc/bin/arm-linux-gnueabihf-g++ -shared -o libbtrack.so kiss_fft.o BTrack.o OnsetDetectionFunction.o


g++ -DUSE_KISS_FFT -c -fPIC BTrack.cpp -o BTrack.o -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -lsamplerate
g++ -DUSE_KISS_FFT -c -fPIC OnsetDetectionFunction.cpp -o OnsetDetectionFunction.o -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -lsamplerate
g++ -shared -o libbtrack.so BTrack.o OnsetDetectionFunction.o



gcc -c -fpic -DUSE_KISS_FFT *.cpp -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -llibsamplerate
gcc -shared -o lib-btrack.so btrack.o


gcc -DUSE_KISS_FFT *.cpp -o btrack.o -stdlib=libstdc++ -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -llibsamplerate

gcc -DUSE_KISS_FFT *.cpp -o btrack.o -stdlib=libstdc++ -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -llibsamplerate
g++ -DUSE_KISS_FFT -c *.cpp -o btrack.o -I../libs/kiss_fft130 -I../../libsamplerate-master/src -L../../libsamplerate-master/build -lsamplerate

popd


COPY . ./

RUN curl -sL https://sentry.io/get-cli/ | bash
RUN apt-get update && apt-get install jq -y
RUN export SENTRY_RELEASE="kmapi-$(jq -r '.FulLSemVer' < ./gitversion.json)"
RUN rm -f ./gitversion.json

RUN dotnet publish KaraokeMaster.BeatDetector -f netcoreapp3.1 -c Release -o out

RUN sentry-cli releases new $SENTRY_RELEASE
RUN sentry-cli releases set-commits $SENTRY_RELEASE --auto
RUN sentry-cli releases finalize $SENTRY_RELEASE



# Runtime

FROM mcr.microsoft.com/dotnet/core:3.1

# Create app directory
WORKDIR /usr/src/karaokemaster-beatdetector

# Copy assets from the build environment
COPY --from=build-env /usr/src/karaokemaster-beatdetector/out ./

ENV COMPlus_DbgEnableMiniDump=1
ENV COMPlus_DbgMiniDumpName=/var/opt/kmdata/coredump.%d

ENTRYPOINT [ "dotnet", "KaraokeMaster.BeatDetector.dll" ]