// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using KaraokeMaster.BeatDetector.Banner;
using KaraokeMaster.BeatDetector.Metrics;
using Loki.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KaraokeMaster.BeatDetector
{
    public class Program
    {
        public static void Main(string[] args)
        {
            StartupBanner.Print(out var sentryRelease);

            CreateHostBuilder(sentryRelease, args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string sentryRelease, string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureAppConfiguration((config) =>
                {
                    config.AddJsonFile("appsettings.json", false)
                        .AddJsonFile("appsettings.local.json", true);
                })
                .ConfigureLogging((hostContext, logging) =>
                {
                    logging.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddLoki(options =>
                    {
                        options.ApplicationName = "KaraokeMaster.BeatDetector";
                        options.IncludeScopes = true;
                        options.IncludePredefinedFields = true;

                        options.AdditionalFields.Add("version", sentryRelease);
                    });
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<MetricsConfiguration>(
                        hostContext.Configuration.GetSection(MetricsConfiguration.ConfigurationKey));

                    services.AddScoped<BeatDetector>();
                    services.AddFactory<EventListener, EventListener>();
                    
                    services.AddHostedService<MetricsWorker>();
                    services.AddHostedService<Worker>();
                });
    }
}