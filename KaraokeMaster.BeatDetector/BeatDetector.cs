// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SoundIOSharp;

namespace KaraokeMaster.BeatDetector
{
    public partial class BeatDetector
    {
        private readonly ILogger<BeatDetector> _logger;

        private readonly SoundIO _api;
        private SoundIODevice _device;
        private SoundIOInStream _stream;
        // private Stopwatch _stopwatch;
        
        private readonly string _configDevice;
        private readonly string _configBackend;
        private readonly bool _configRaw = false;
        private readonly int _configFrameSize = 0;
        private readonly byte _which = 1;

        public BeatDetector(IConfiguration config, ILogger<BeatDetector> logger)
        {
            _logger = logger;

            _configDevice = config["SoundDevice"];
            _configBackend = config["SoundBackend"];
            bool.TryParse(config["SoundRaw"], out _configRaw);
            int.TryParse(config["SoundFrameSize"], out _configFrameSize);
            // Make sure the frame size is even...
            _configFrameSize += _configFrameSize % 2;
            if (_configFrameSize < 512) _configFrameSize = 1024;

            byte.TryParse(config["Which"], out _which);
            
            _api = new SoundIO();
            // _stopwatch = Stopwatch.StartNew();
            
            ConfigureBackend();
            ConfigureDevice();
        }

        public Action<double> BeatCallback { get; set; }
        
        private void ConfigureBackend()
        {
            var backend = _configBackend == null ? SoundIOBackend.None : (SoundIOBackend)Enum.Parse (typeof (SoundIOBackend), _configBackend);
            if (backend == SoundIOBackend.None)
                _api.Connect ();
            else
                _api.ConnectBackend (backend);
            _logger.LogInformation($"Using backend: {_api.CurrentBackend}");
            
            _api.FlushEvents ();

            Enumerable.Range(0, _api.InputDeviceCount)
                .Select(i => _api.GetInputDevice(i).Id)
                .ToList()
                .ForEach(d => _logger.LogInformation($"Available device: {d}"));
        }

        private void ConfigureDevice()
        {
            _device = _configDevice == null ? _api.GetInputDevice (_api.DefaultInputDeviceIndex) :
                Enumerable.Range (0, _api.InputDeviceCount)
                    .Select (i => _api.GetInputDevice (i))
                    .FirstOrDefault (d => d.Id == _configDevice && d.IsRaw == _configRaw);
            if (_device == null) {
                _logger.LogCritical($"device {_configDevice} not found.");
                throw new Exception("Bad");
            }
            _logger.LogInformation($"Using device: {_device.Name}");
            if (_device.ProbeError != 0) {
                _logger.LogCritical($"Cannot probe device {_configDevice}.");
                throw new Exception("Bad as well");
            }
        }

        private void ConfigureStream()
        {
            var sample_rate = prioritized_sample_rates.First (sr => _device.SupportsSampleRate (sr));

            var fmt = prioritized_formats.First (f => _device.SupportsFormat (f));

            _stream = _device.CreateInStream ();
            _stream.Format = fmt;
            _stream.SampleRate = sample_rate;
            _stream.ReadCallback = (fmin, fmax) => read_callback (_stream, fmin, fmax);
            _stream.OverflowCallback = () => overflow_callback (_stream);

            _stream.Open ();

            _logger.LogInformation($"sample rate: {sample_rate}, format: {fmt}, bytes/frame: {_stream.BytesPerFrame}");
            const int ring_buffer_duration_seconds = 30;
            int capacity = (int)(ring_buffer_duration_seconds * _stream.SampleRate * _stream.BytesPerFrame);
            _logger.LogInformation("capacity: " + capacity.ToString());
            ring_buffer = _api.CreateRingBuffer (capacity);
        }

        public void Start(CancellationToken stoppingToken)
        {
            switch (_which)
            {
                case 3:
                    Start3(stoppingToken);
                    break;
                default:
                    Start1(stoppingToken);
                    break;
            }
        }
        
        private void Start1(CancellationToken stoppingToken)
        {
            ConfigureStream();
            
            _logger.LogInformation("Starting beat detection...");
            
            _stream.Start ();

            int blockSize = _configFrameSize * sizeof(double);

            using (var btrack = new BTrack.BTrack(_configFrameSize / 2, _configFrameSize, _stream.SampleRate))
            {
                var arr = new byte [blockSize];
                unsafe
                {
                    fixed (void* arrptr = arr)
                    {
                        while (!stoppingToken.IsCancellationRequested)
                        {
                            _api.FlushEvents();
                            while (ring_buffer.FillCount < blockSize)
                            {
                                Thread.Sleep(100);
                            }

                            int fill_bytes = Math.Min(blockSize, ring_buffer.FillCount);
                            var read_buf = ring_buffer.ReadPointer;

                            Buffer.MemoryCopy((void*) read_buf, arrptr, fill_bytes, fill_bytes);

                            List<double> frame = new List<double>();
                            // interpret as 16 bit audio... Fill the list with samples (LRLRLRLR...)
                            for (int index = 0; index < blockSize; index += 2)
                            {
                                short sample = (short)((arr[index + 1] << 8) | arr[index + 0]);
                                // to floating point
                                var sample32 = sample / 32768.0;
                                frame.Add(sample32);
                            }

                            // Convert list to array
                            var framearray = frame.ToArray();

                            // some manual garbage collection
                            GCHandle handle = GCHandle.Alloc(framearray, GCHandleType.Pinned);
                            fixed (double* framePtr = framearray)
                            {
                                // call ProcessAudioFrame - uses IntPtr to access the sample array
                                btrack.ProcessAudioFrame(ref *framePtr);
                            }
                            
                            framearray = null;
                            frame.Clear();
                            handle.Free();
							
                            ring_buffer.AdvanceReadPointer(fill_bytes);

                            if (btrack.BeatDueInCurrentFrame)
                            {
                                _logger.LogTrace("Bizzeat");
                                // _stopwatch.Stop();
                                
                                BeatCallback?.Invoke(btrack.CurrentTempoEstimate);
                                // _logger.LogInformation($"{_stopwatch.ElapsedMilliseconds} ms elapsed, {TimeSpan.FromMinutes(1) / _stopwatch.Elapsed} apparent bpm, {btrack.CurrentTempoEstimate} reported bpm");
                                // _stopwatch = Stopwatch.StartNew();
                            }
                        }
                    }
                }
            }

            _stream.Dispose ();
            _device.RemoveReference ();
            _api.Dispose ();
        }

        private void Start3(CancellationToken stoppingToken)
        {
            ConfigureStream();
            
            _logger.LogInformation("Starting beat detection...");
            
            _stream.Start ();

            int blockSize = _configFrameSize * sizeof(double);

            using (var btrack = new BTrack.BTrack(_configFrameSize / 2, _configFrameSize, _stream.SampleRate))
            {
                var arr = new byte [blockSize];
                var arrDbl = new double [blockSize / 2];
                unsafe
                {
                    fixed (void* arrptr = arr)
                    fixed (double* dblptr = arrDbl)
                    {
                        while (!stoppingToken.IsCancellationRequested)
                        {
                            _api.FlushEvents();
                            while (ring_buffer.FillCount < blockSize)
                            {
                                Thread.Sleep(100);
                            }

                            int fill_bytes = Math.Min(blockSize, ring_buffer.FillCount);
                            var read_buf = ring_buffer.ReadPointer;

                            Buffer.MemoryCopy((void*) read_buf, arrptr, fill_bytes, fill_bytes);

                            // interpret as 16 bit audio... Fill the list with samples (LRLRLRLR...)
                            for (int index = 0; index < blockSize; index += 2)
                            {
                                short sample = (short)((arr[index + 1] << 8) | arr[index + 0]);
                                // to floating point
                                var sample32 = sample / 32768.0;
                                arrDbl[index / 2] = sample32;
                            }

                            btrack.ProcessAudioFrame(ref *dblptr);
							
                            ring_buffer.AdvanceReadPointer(fill_bytes);

                            if (btrack.BeatDueInCurrentFrame)
                            {
                                BeatCallback?.Invoke(btrack.CurrentTempoEstimate);
                            }
                        }
                    }
                }
            }

            _stream.Dispose ();
            _device.RemoveReference ();
            _api.Dispose ();
        }
    }
}