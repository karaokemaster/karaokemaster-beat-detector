// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;

namespace KaraokeMaster.BeatDetector
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly BeatDetector _detector;
        private readonly HubConnection _connection;
        private bool _playing = true;
        
        public Worker(IConfiguration config, ILogger<Worker> logger, BeatDetector detector)
        {
            _logger = logger;
            _detector = detector;

            var configUrl = config["KaraokeMasterUrl"];
            _logger.LogInformation($"Configured URL: `{configUrl}`");
            var baseUrl = new Uri(configUrl);
            var hubUrl = new Uri(baseUrl, "api/hub");

            _logger.LogInformation($"Using hub URL `{hubUrl}`");
            
            _connection = new HubConnectionBuilder()
                .WithUrl(hubUrl)
                // TODO: Implement IRetryPolicy to be more smarter
                .WithAutomaticReconnect()
                .Build();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _connection.Closed += async (error) =>
            {
                _logger.LogWarning($"Connection failed! {error}");
                await Task.Delay(new Random().Next(0,5) * 1000, stoppingToken);
                await _connection.StartAsync(stoppingToken);
            };

            _connection.Reconnecting += async error =>
            {
                _logger.LogWarning($"Connection reconnecting! {error}");
            };

            _connection.Reconnected += async s =>
            {
                _logger.LogWarning($"Connection reconnected. s? s! {s}");
            };
            
            // TODO: An observable would be much better here
            _connection.On<bool>("PlayerState", playing =>
            {
                if (_playing != playing)
                    _logger.LogInformation($"PlayerState is now {playing}");
                _playing = playing;
            });

            _connection.On<long, bool>("BeatEcho", async (timestamp, echo) =>
            {
                if (!echo) return;
                
                _logger.LogInformation($"BeatEcho received with timestamp {timestamp}");

                try
                {
                    await _connection.InvokeAsync("BeatEcho", timestamp, false, cancellationToken: stoppingToken);
                    _logger.LogInformation($"BeatEcho with timestamp {timestamp} returned");
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error sending BeatEcho message: {e}");
                }
            });

            _detector.BeatCallback = async (tempo) =>
            {
                try
                {
                    _logger.LogTrace($"Beat! Tempo @ {tempo:0} bpm {(_playing ? "SUPPRESSED" : "")}");
                    if (_playing && _connection.State == HubConnectionState.Connected)
                        await _connection.InvokeAsync("Beat", tempo, stoppingToken);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error sending Beat message: {e}");
                }
            };
            
            try
            {
                _logger.LogInformation("Connecting...");
                await _connection.StartAsync(stoppingToken);
                _logger.LogInformation("Connection started");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            
            _detector.Start(stoppingToken);
        }
    }
}