// Copyright 2019-2021 Mike Peschka
// 
// This file is part of KaraokeMaster.
// 
// KaraokeMaster is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// KaraokeMaster is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Runtime.InteropServices;
using SoundIOSharp;

namespace KaraokeMaster.BeatDetector
{
    public partial class BeatDetector
    {
        static SoundIORingBuffer ring_buffer = null;

        static SoundIOFormat [] prioritized_formats = {
            SoundIODevice.Float32NE,
            SoundIODevice.Float32FE,
            SoundIODevice.S32NE,
            SoundIODevice.S32FE,
            SoundIODevice.S24NE,
            SoundIODevice.S24FE,
            SoundIODevice.S16NE,
            SoundIODevice.S16FE,
            SoundIODevice.Float64NE,
            SoundIODevice.Float64FE,
            SoundIODevice.U32NE,
            SoundIODevice.U32FE,
            SoundIODevice.U24NE,
            SoundIODevice.U24FE,
            SoundIODevice.U16NE,
            SoundIODevice.U16FE,
            SoundIOFormat.S8,
            SoundIOFormat.U8,
            SoundIOFormat.Invalid,
        };

        static readonly int [] prioritized_sample_rates = {
            48000,
            44100,
            96000,
            24000,
            0,
        };

        static void read_callback (SoundIOInStream instream, int frame_count_min, int frame_count_max)
        {
            var write_ptr = ring_buffer.WritePointer;
            int free_bytes = ring_buffer.FreeCount;
            int free_count = free_bytes / instream.BytesPerFrame;

            if (frame_count_min > free_count)
                throw new InvalidOperationException ("ring buffer overflow"); // panic()

            int write_frames = Math.Min (free_count, frame_count_max);
            int frames_left = write_frames;

            for (; ; ) {
                int frame_count = frames_left;

                var areas = instream.BeginRead (ref frame_count);

                if (frame_count == 0)
                    break;

                if (areas.IsEmpty) {
                    // Due to an overflow there is a hole. Fill the ring buffer with
                    // silence for the size of the hole.
                    for (int i = 0; i < frame_count * instream.BytesPerFrame; i++)
                        Marshal.WriteByte (write_ptr + i, 0);
                    Console.Error.WriteLine ("Dropped {0} frames due to internal overflow", frame_count);
                } else {
                    for (int frame = 0; frame < frame_count; frame += 1) {
                        int chCount = instream.Layout.ChannelCount;
                        int copySize = instream.BytesPerSample;
                        unsafe {
                            for (int ch = 0; ch < chCount; ch += 1) {
                                var area = areas.GetArea (ch);
                                Buffer.MemoryCopy ((void*)area.Pointer, (void*)write_ptr, copySize, copySize);
                                area.Pointer += area.Step;
                                write_ptr += copySize;
                            }
                        }
                    }
                }

                instream.EndRead ();

                frames_left -= frame_count;
                if (frames_left <= 0)
                    break;
            }

            int advance_bytes = write_frames * instream.BytesPerFrame;
            ring_buffer.AdvanceWritePointer (advance_bytes);
        }

        static int overflow_callback_count = 0;
        static void overflow_callback (SoundIOInStream instream)
        {
            Console.Error.WriteLine ("overflow {0}", overflow_callback_count++);
        }        
    }
}